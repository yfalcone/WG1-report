%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Runtime Assertion Checking and Runtime Verification}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%
Another area strongly connected to runtime verification (RV) is \emph{runtime assertion checking} (RAC).
%
Strictly speaking, RAC is a special case of RV.
%
Nonetheless, the characteristics are so different from most approaches to RV that RAC is sometimes seen as a field of its own.
%
In any case, we here take the approach which classifies RAC as an RV technique, whereas the other, mainstream approaches to RV are identified as \emph{runtime trace checking} (RTC).
%
We give an overview of this classification in Fig.~\ref{fig:camparetechniques}, also relating it to the major areas of static verification, model checking and deductive verification.
%
Note that we are deliberately simplifying the presentation.
%
Doing full justice to all verification approaches is not in scope of this section.
%
Rather we want to exhibit some basic characteristics.

\begin{table}[t]
\caption{Comparison of Verification Techniques (simplified)}
\centering
\begin{tabular}{|@{\hspace{1ex}}c@{\hspace{1ex}}|@{\hspace{1ex}}c@{\hspace{1ex}}|c|c|}
  \hline
  \textbf{Runtime Verification} & \textbf{Static Verification} & \textbf{Properties} & \textbf{Specifications}\\
  \hline
  \hline
  \specialcell{Runtime\\ Trace\\ Checking} & \specialcell{Model\\ Checking}
                                 & \specialcell{valid traces\\ \footnotesize{(+ some data)}}
                                 & \specialcell{temporal logics,\\ automata,\\ regular languages\\ (+ extensions)}\\
  \hline
  \specialcell{Runtime\\ Assertion\\ Checking} & \specialcell{Deductive\\ Verification}
                                         & \specialcell{valid data\\ in specific\\ code locations\\ \footnotesize{(+ some trace info)}}
                                         & \specialcell{first-order\\ assertion languages\\ (+ extensions)}\\
  \hline
\end{tabular}
\label{fig:camparetechniques}
\end{table}

RV mostly focuses on properties of execution \emph{traces} of some system, so most of RV fall under runtime trace checking (RTC).
%
These properties may be specified using different formalisms, of which temporal logics, automata, and regular languages are prominent examples.
%
In what concerns properties and specification approaches, RTC has therefore similarities with Model Checking. Most of this entire document is about RTC.

On the other hand, properties which are typically addressed by runtime assertion checking (RAC) focus on conditions on the \emph{data}, in specific code locations.
%
RAC comes with assertion languages, to formulate assertions to be `placed' at source code locations.
%
These assertions often use concepts from first-order logic to constrain which data is valid in the respective code locations.
%
RAC is therefore richer than RTC when it comes to properties of the data, but less expressive when it comes trace related properties.

A prominent example of a runtime assertion checker is OpenJML\footnote{\url{www.openjml.org}}, which supports the checking of JML~\cite{Huismanetal16} assertions while executing (instrumented) Java applications.
%
Other examples of RAC tools are SPARK~\cite{Barnes:2012:SPA:2436812}, and SPEC\#~\cite{Boogie04}, supporting variants of Ada and C\#, respectively.

One has to say that RAC techniques, because of the expressiveness of the used specification languages, are often too slow to be used for post-deployment RV, and are therefore better suited for the debugging phase.
%
This may be another reason why RAC is not always counted as an RV technique.
%
However, recent developments show that RAC can be very much optimized by combining static and runtime verification~\cite{AhrendtCPS17}.
%
%
