%%%%%%%%%%%%%
\subsubsection{Model Checking and Runtime Verification}
%
A popular verification technique besides runtime verification is \emph{model checking}~\cite{DBLP:books/daglib/0007403}.
%
While runtime verification checks a single execution of the underlying system, model checking considers all possible executions of the underlying system.
%
As such, both techniques share a common goal, i.e., the verification of an underlying system, but they have different features.
%
In general, the two techniques may be combined.
%
Let us briefly discuss the methodological combinations of the two techniques as well as their formal relationship.

At a first sight, model checking seems to provide stronger correctness guarantees than runtime verification as model checking considers all possible executions of a system.
%
However, model checking still greatly suffers from the so-called state-space explosion problem limiting its application only to parts of or abstractions of an underlying system.
%
As such, it makes perfectly sense to combine both verification techniques.
%
In \cite{DBLP:journals/jlp/LeuckerS09}, several combinations are discussed, which we briefly summarize here. 
%
\begin{itemize}
\item The verification result obtained by model checking is often referring to a model of the real system under analysis but not to the actual byte code, CPU etc.
%
\item However, the implementation might behave slightly different than predicted by the model. Runtime verification may then be used to easily check the actual execution of the system, to make sure that the implementation really meets its correctness properties.
	Thus, runtime verification may act as a partner to model checking.
\item
Often, some information is available only at runtime or is conveniently checked at runtime. For example, whenever library code with no accompanying source code is part of the system to build, only a vague description of the behavior of the code might be available. In such cases, runtime verification is an alternative to model checking.
\item The behavior of an application may depend heavily on the environment of the target system, for which certain assumptions have been taken at model checking time. Runtime verification allows to check such assumptions at runtime and may raise an alarm if one of the assumptions does not hold. Here, runtime verification completes the overall verification. 
\item In the case of systems where security is important or in the case of safety-critical systems, it is useful also to monitor behavior or properties that have been statically proved or tested, mainly to have a double check that everything goes well: Here again, runtime verification acts as a partner of model checking.
\end{itemize}
%
However, besides the methodological combination of runtime verification and model checking, there is a formal combination possible, as described in \cite{DBLP:conf/rv/Leucker12}. Let us recall the main idea here while we refer to the previously mentioned reference for formal details. 

Especially in online runtime verification, an execution of the underlying system is checked against a correctness property letter by letter. To reuse for example linear-time temporal logic (LTL) defined over infinite traces \cite{DBLP:conf/focs/Pnueli77}, the verdict for the finite execution is obtained by considering all possible infinite continuations of the execution. If with all such extensions the LTL formula yield either only \emph{true} or only \emph{false}, the corresponding verdict is that for the finite execution seen so far. Otherwise, the verdict is \emph{don't know?} to signal that the current excution does not allow a precise verdict anymore. This approach yields the so-called \emph{anticipatory} semantics introduced in \cite{DBLP:journals/tosem/BauerLS11}. 

In \cite{DBLP:conf/rv/Leucker12}, the idea was pursued that rather considering all possible infinite extensions of the current execution, only those are taken into account that yield runs of the overall system. Of course, a system model has to be at hand to understand which extension are possible at all. However, let us consider this idea for the empty execution, i.e., when the system has not even started: Then, we have to check whether all runs of the underlying system either satisfy or falsify the property at hand. The first question is actually the model-checking problem, and of course, if we have answered the model checking problem before we even start the execution of the system, the need for runtime verification vanishes. Now, assume that we consider an over-approximation of the underlying system, that is, a system having more runs/behavior, but smaller/less states. We still implicitly solve the model checking problem, indeed after every partial execution, yet for an abstract system. Thus, if the answer is that there is no violation possible any more (\emph{true}), one can stop monitoring. If, however, the model checking answer is \emph{false}, there might be a bug in the system, which may be found by further monitoring. 
Hence, by considering an over-approximation of the underlying system, we get best out of both worlds: we combine efficient runtime verification techniques with taking part of the system into account, hereby sharpening the verdict obtained by runtime verification. By tuning the abstraction, we can adjust whether the focus is on model checking or rather on runtime verification. In other words, by controlling the abstraction, we are able to slide between model checking and runtime verification. See \cite{DBLP:conf/rv/Leucker12} for further details. 
%
%
%