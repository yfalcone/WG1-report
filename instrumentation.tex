%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Instrumentation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
The term instrumentation refers to the mechanism employed to probe and to extract signals, traces of events and other information of interest from a software or hardware system during its execution.
%
Instrumentation determines what aspects of the system execution are made visible (to the monitor) for analysis, in terms of what elements of the computation is reported (e.g., computation steps and the data associate with them), and the relationships between the recorded events (e.g. provide a partial or total ordering, or guaranteeing that the reported event order corresponds to the order in which the respective computational step occurred).
%
Instrumentation also dictates how the system and the monitor execute in relation to one another in a monitoring setup.
%
It may either require the system to terminate executing before the monitor starts running (offline), interleave the respective executions of the system and the monitor within a common execution thread (online), or allocate the monitor and the system separate execution threads (inline vs outline); instrumentation may dictates how tightly coupled these executions need to be (synchronous vs asynchronous).  

%\subsection{Mechanisms to access data}

The choice of instrumentation techniques depends on the type of system to be monitored.
%
For example, monitoring hardware system may require probing mixed-analog signals using physical wires, while for software the instrumentation method is strictly related to the programming language in which the software is implemented or to the low-level language in which it is compiled (i.e., bytecode, assembly, etc.).

The following instrumentation mechanisms exist:

\subsubsection{Logging or manual instrumentation}

Most systems already log important actions. Typically, the level of detail
of these logs is configurable. When a system is configured to log data
a high level of detail, the log may
contain enough information to derive a verification verdict from its
data~\cite{jepsen,naldurg2004temporal,hu2011automating}.
If this is not the case, additional data has to be
obtained by manual instrumentation (manually inserting logging code).
Furthermore, the format of the data is typically unstructured or semi-structured,
and typically have to be pre-processed before they can be used for
monitoring~\cite{zhu2010incremental,tang2011logsig}.
The advantages of this approach are that the data is typically already
available, and no special tools have to be set up.

\subsubsection{Code instrumentation (transpilation/weaving)}

With code instrumentation, the code of the program is
modified such that the necessary statements to capture
the information for runtime monitoring are inserted. This modification
is usually automatic and supported by a tool. This has the
advantage that it is possible to cover similar properties in a uniform
way across a large program, as the code is modified in a systematic
and automatic way. Furthermore, this technique is almost unlimited
w.\,r.\,t.\ the amount and type of data it can access, as code
instrumentation can be very fine-grained.

The tools that modify the code are often called transpilers (if the source
code is modified) or weavers (if source or compiled code is modified).
Transpilation occurs right before compilation, whereas compiled code
can be modified after compilation, or at load time. In all of these cases,
the interplay of various stages of building and deployment of software
may not readily accept a code instrumentation stage without adaptation,
due to actions such as code generation, dynamic loading of libraries,
other code rewriting frameworks such as Spring~\cite{johnson2004spring}, etc.
While the result of source-to-source transformed can be readily inspected,
weavers for compiled code are more flexible and also work without the source.
However, it can be difficult to ensure that all information is captured
fully and correctly, without altering the original behavior of the program.

Popular transpilers can be implemented as libraries~\cite{pawlak:hal-01169705}, term
rewriting systems \cite{balland2007tom,visser2001stratego},
compiler extensions~\cite{lee2003cetus,gcc-instr},
domain-specific languages~\cite{klint2009rascal},
or even transformation generators~\cite{kuipers2001object}.
Weavers include domain-specific languages such as AspectJ~\cite{kiczales2001overview} or AspectC++\cite{spinczyk2002aspectc++},
but also libraries such as ASM~\cite{bruneton02}.
Code instrumentation is also often provided by custom tools that have
been designed with the given verification task in
mind~\cite{rubanov2011runtime,nethercote2007valgrind,Savage:1997:EDD:269005.266641}.

\subsubsection{Call interception}

When analyzing activity such as input/output, which is accessed via
existing libraries, the easiest way to observe these actions is by
overloading the libraries with a wrapper. The wrapper then intercepts
the call and updates
the runtime monitor before or after calling the original library code.
This approach differs from code instrumentation in that code is not
modified throughout the system, but instead, the modification is
made by intercepting calls systematically, and providing additional
functionality before or after each function call.

Typical mechanisms to achieve this are the usage of
the linker to replace a library call with a wrapper~\cite{gcc-link},
kernel modifications~\cite{artho2015using},
the boot-classpath in Java (up to Java 8)~\cite{java8-bootcp}
or Java's module system~\cite{java-modules}. This approach is less
flexible than other approaches, in that it can only modify the
behavior of code at function call boundaries,
but it is easy to confine the
modifications to small parts of the system. However, not all platforms
have a straightforward mechanism of keeping the unmodified version
and delegating a call to the original code inside the wrapper;
for instance, older versions of Java completely replaced the overloaded
library without any way of accessing the old code. Furthermore, the mechanisms
to overload libraries are very specific to each platform, so tools
using this technique are not portable.

\begin{table}[t]
\caption{Advantages and disadvantages of different approaches\label{tab:instr}}
\centering
\begin{tabular}{l l}
\hline
\textbf{Approach} & \textbf{Advantages and disadvantages}\\
\hline
Logging& $+$ Much data already available; no special tooling needed.\\
& $-$ Data format is unstructured; available data may be limited; ad hoc.\\
\hline
Code instrumentation& $+$ Systematic, flexible approach.\\
& $-$ Difficult to use and test; may interfere with original program.\\
\hline
Call interception& $+$ Limited modifications; good performance.\\
&$-$ Not portable; limited to the interfaces of libraries.\\
\hline
Execution environment& $+$ Fast; no modification of the program needed.\\
&$-$ Difficult; not portable; limited to given data.\\
\hline
\end{tabular}
\end{table}

\subsubsection{Execution environment}

Many execution environments have interfaces with events from program
execution can be obtained. These interfaces include the Java Virtual
Machine Tool Interface (JVMTI~\cite{jvmti}) and
the LTTng interface for the Linux kernel~\cite{desnoyers2006lttng}.
Typically, the use of these mechanisms requires access to the data
structures of the execution environment itself. Monitoring code is
therefore not written at the level of the ``guest'' language (such
as Java in the case of JVMTI), but at the level of the ``host''
language, the language in which the virtual machine is written in.
This makes such monitoring approaches harder to use and less portable.
Finally, execution events may even be generated by custom hardware.

This technique has the advantage that it works in an unmodified execution
environment, and typically offers the least amount of overhead of all
approaches. However, even though modern environments offer a large range
of data through these tool interfaces, they are still restricted to
a predefined set of data.

Therefore, the option to use a special or modified execution environment
is sometimes used. A modified execution environment may change parts of
the kernel~\cite{artho2015using} or use a specific virtual machine that generates
a wider range of events~\cite{visser2003model}, or a debugger that can inspect
(and even modify) data at a finer level of detail than otherwise
possible~\cite{jakse2017interactive}.
Unlike standard environments, performance and stability of runtime
monitoring in these special environments depend on the modifications
needed to obtain the data.

Finally, a non-standard execution environment may even involve
special hardware access ports designed for monitoring~\cite{jtag}.
In this case, there is often no overhead, and at the software layer,
the execution environment is indistinguishable from a standard
environment.

Table~\ref{tab:instr} gives an overview of the different types of
approaches, and their pros and cons.
In~\cite{BartocciFFR18}, we further explain these concepts in two dedicated sections for hardware and software instrumentation.
