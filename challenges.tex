\section{Challenges in Monitoring Quantitative and Statistical Data, beyond Property Violation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
Since runtime verification has traditionally borrowed techniques, and in particular specification languages, from static verification the specifications from which monitors are extracted are typically temporal logics and similar formalisms.
%
In turn, the outcome of a monitoring process is typically a Boolean verdict (or sometimes an enriched Boolean verdict to convey non-definite answers).
%
At the same time, decision problems on specifications (like equivalence vacuity, entailment, etc.) are decidable as these problems have been studied in static verification.
%
However, if one is willing to sacrifice the decidability of the these decision procedures, there is the possibility of designing “richer” logics.
%
One can perform runtime verification activities as long as there is a formal procedure to generate monitors from specifications, and an associated evaluation method for evaluating the generated monitors against an input trace.
%
Taking this abstract viewpoint on monitoring allows to consider languages that process rich data and generate richer outcomes. 

Consequently, we suggest here two directions in which runtime verification can be extended in terms of how rich the data that the monitors handle is.
%
The first direction is about the data the monitor processes and manipulates.
%
The second direction is about richer outcomes from the monitoring process.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Richer Data for Monitors}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
Two research areas related to runtime verification that study how to handle sequences of events with rich data, and that can produce rich data as outcome, are Complex Event Processing (CEP) and Data Stream Management Systems (DSMS); see~\cite{CugolaM12} for a modern survey.
%
Complex Event Processing considers distributed system that processes flows of events from different sources, finding correlations and producing derived events as a result. 
%
The main difference between CEP and RV (even the vision of RV with rich data that we advocate in this section) is that CEP does not provide a formal specification language with formal semantics, but instead an infrastructure to evaluate event processors. 
%
Similarly, DSMS borrow many techniques from Data Bases with the main difference that queries are supposed to process the flow of events without storing all events (even though many DSMS do create a storage with a time index).
%
Typically, DSMS process aggregations over flows of input events where the main concerns are efficiency in the evaluation process and statistical properties of the output (instead of logical correctness). 
%
One can potentially map the formal specifications that we use to generate monitors in RV to queries (for DSMS) and  to processors (in CEP), these areas do not provide formal translations nor a formal semantics of the execution platforms with guarantees on the order of arrival of events.

The book chapter “Monitoring Events that Carry Data”~\cite{HavelundRTZ18} 
%part of the book ``Lectures on Runtime Verification"
\cite{DBLP:series/lncs/10457} reviews the research landscape on runtime verification techniques for traces that contain rich data, but we envision that richer and richer extensions will be investigated in years to come.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Richer Outcomes from the Monitoring Process}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
Another important aspect that has not been extensively studied in runtime verification is richer verdicts.
%
If one considers monitors as transformers from traces into useful “summaries” of information about the trace, there are many possibilities.
%
For example, in the area of signal temporal logics, there is the notion of “robustness”, which is a quantity that measures “how much” an observed trace matches a given specification (in specialized logics for the domain of signal processing like STL, MTL, etc.).
%
A value of 0 can indicate that  a full violation is detected, while values close to 0 (like 0.4) can indicate that a violation is close to occur.
%
This richer outcome allows to determine hot robustly a trace is satisfied or violated.
%
Potentially, the outcome of a monitor could be a pruned trace that allows further analysis to be performed, for example to determine the root of the error.
%
Finally, there are few works that consider the possibility of processing (statistically) incorrect or missing data.
%
One challenge for the near future is to explore different rich outcomes that the monitors can generate and the trade-offs involved in the evaluation of these monitors.

One potential direction to attack this challenge is Stream Runtime Verification (SRV), which was conceived to trigger richer verdicts beyond YES/NO answers.
%
SRV, pioneered by the tool Lola~\cite{DAngeloSSRFSMM05}, proposes to use streams to separate two concerns: the algorithms to perform evaluations of temporal properties on input traces, and the data collected during the evaluation.
%
The key insight is that many algorithms, for example to check LTL properties (and similar formalisms proposed to express monitors in the RV community) can be easily generalized to compute richer verdicts.
%
As a simple example, one can use the same algorithm that searches for a violating position in the past to compute the number of offending positions.
%
Similarly, one can compute the longest response time to a request, or the average response time (and not just the fact that all requests are answered). 
%
Modern extensions of stream runtime verification enrich the basic setting to parameterized properties~\cite{FaymonvilleFST16} so the input stream can be classified according to different objects.
%
Applications include network monitoring~\cite{FaymonvilleFST16}, security assurance of unmanned aerial vehicles~\cite{AdolfFFST17} and real-time SRV for the dynamic analysis of timed-event streams from low-level concurrent software~\cite{LeuckerSS0S18}.
%
All these results were motivated by challenging domains for which the ability of SRV to handle quantitative data and produce rich verdicts proved to be valuable.
%

Another potential direction to attack this challenge consists in using \emph{decentralised specifications}~\cite{El-HokayemF17}, pioneered by the tool THEMIS~\cite{El-HokayemF17a}.
%
While simple specifications can be expressed with traditional specification formalisms such as LTL and automata, accounting for hierarchies quickly becomes a problem.
%
Informally, a decentralized specification considers the system as a set of components, defines a set of monitors, additional atomic propositions that represent references to (the verdict of other) monitors, and attaches each monitor to a component.
%
Each monitor is a Moore automaton where the transition label is restricted to only atomic propositions related to the component on which the monitor is attached, and references to other monitors.
%
Decentralized specifications were successfully applied to the monitoring of smart homes~\cite{El-HokayemF18a} where they proved to allow a better scaling than with traditional specification formalisms.